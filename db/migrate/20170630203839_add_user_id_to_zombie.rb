class AddUserIdToZombie < ActiveRecord::Migration[5.0]
  def change
    add_column :zombies, :user_id, :integer
  end
end
